CREATE TABLE therapists(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
)

CREATE TABLE daily_work_shifts(
    id  INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    therapist_id INT(11) NOT NULL,
    target_date DATE NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
)
