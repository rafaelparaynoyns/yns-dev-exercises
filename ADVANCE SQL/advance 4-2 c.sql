SET @StartTIME = '05:59:59';
SET @EndTIME = '00:00:00';

SELECT daily.therapist_id, the.name, daily.target_date, daily.start_time, daily.end_time,
CASE 
    WHEN (daily.start_time <= @StartTIME AND daily.start_time >= @EndTIME) THEN CONCAT(DATE_ADD(target_date, INTERVAL 1 DAY), ' ', daily.start_time) 
ELSE CONCAT(target_date, ' ', daily.start_time) 
END AS sort_start_time 
FROM daily_work_shifts AS daily 
INNER JOIN therapists AS the ON daily.therapist_id = the.id 
ORDER BY daily.target_date, sort_start_time;