INSERT INTO therapists (name) 
VALUES
('John'),
('Arnold'),
('Robert'),
('Ervin'),
('Smith');
-- INSERT INTO daily_work_shifts TABLE
INSERT INTO daily_work_shifts (therapist_id, target_date, start_time, end_time) 
VALUES
(1, '2021-09-09', '14:00:00', '15:00:00'),
(2, '2021-09-09', '22:00:00', '23:00:00'),
(3, '2021-09-09', '00:00:00', '01:00:00'),
(4, '2021-09-09', '05:00:00', '05:30:00'),
(1, '2021-09-09', '21:00:00', '21:45:00'),
(5, '2021-09-09', '05:30:00', '05:50:00'),
(3, '2021-09-09', '02:00:00', '02:30:00');
