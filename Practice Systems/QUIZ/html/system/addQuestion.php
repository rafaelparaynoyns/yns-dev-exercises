<?php
require_once './functions.php';

if (isset($_GET['question_id'])) {
  $questionData = $questiondb->getDatabyId($_GET['question_id']);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
  $valid = $questionErr = $choice1Err =  '';
  extract($_POST);

  if (empty($question)) {
    $questionErr = "question is Reqsuired";
  } else {
    $questionErr = true;
  }

  if ($questionErr == 1) {
    if ($question_id != null) {
      $questiondb->editQuiz_question($question_id, $question, $opt1, $opt2, $opt3, $correct_answer);
    } else {
      $questiondb->addQuiz_question($id, $question, $opt1, $opt2, $opt3, $correct_answer);
    }
    header("Location:question.php?id=$id");
    exit;
  }
}
require_once './templates/header.php';
require_once './templates/navigation.php';
?>

<div class="form">

  <p class="sign" align="center">Add Question</p>
  <form class="form1" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <input type="hidden" name="id" value="<?php if (isset($_GET['id'])) {
                                            echo $_GET['id'];
                                          } ?>" />
    <input type="hidden" name="question_id" value="<?php if (isset($_GET['question_id'])) {
                                                      echo $_GET['question_id'];
                                                    } ?>" />
    <input class="input" name="question" type="text" align="center" placeholder="Question" value="<?php if (isset($_GET['question_id'])) {
                                                                                                    echo $questionData['question'];
                                                                                                  } ?>" />
    <div class="listChoices">
      <div class="my-auto">
        <label class="label-input">
          Choice 1
        </label>
        <input type="text" name="opt1" id="choice1" class="input" value="<?php if (isset($_GET['question_id'])) {
                                                                            echo $questionData['opt1'];
                                                                          } ?>" />

        <div class="w-full flex justify-end">
        </div>
      </div>

      <div class="my-auto">
        <label class="label-input">
          Choice 2
        </label>
        <input type="text" name="opt2" id="choice2" class="input" value="<?php if (isset($_GET['question_id'])) {
                                                                            echo $questionData['opt2'];
                                                                          } ?>" />

        <div class="w-full flex justify-end">

        </div>
      </div>
      <div class="my-auto">
        <label class="label-input">
          Choice 3
        </label>
        <input type="text" name="opt3" id="choice3" class="input" value="<?php if (isset($_GET['question_id'])) {
                                                                            echo $questionData['opt3'];
                                                                          } ?>" />
      </div>
      <input class="input" name="correct_answer" type="text" align="center" placeholder="correct answer" value="<?php if (isset($_GET['question_id'])) {
                                                                                                                  echo $questionData['correct_answer'];
                                                                                                                } ?>" />
    </div>

    <input type="submit" value="Save" class="submit" align="center">

</div>

<?php require_once './templates/footer.php'; ?>