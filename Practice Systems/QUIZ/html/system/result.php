<?php
require_once './functions.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {
  $qno = $_GET['qno'];
  $correctAnswer = 0;

  for ($i = 0; $i < $qno; $i++) {
    if ($_POST['choice' . $i] === $_POST['correct_answer' . $i]) {
      $correctAnswer++;
    }
  }
}
require_once './templates/header.php';
require_once './templates/navigation.php';
?>
<div class="takequiz">
  <h2 style="text-align: center;">Total Score: <?= $correctAnswer ?>/<?= $_GET['qno'] ?></h2>
</div>

<?php require_once './templates/footer.php'; ?>