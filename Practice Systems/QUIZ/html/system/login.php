<?php
require_once './functions.php';
session_start();
if (isset($_SESSION['user'])) {
  header('Location: ./dashboard.php');
  exit;
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
  if (isset($_POST['loginSumb'])) {
    $log = true;
    $valid = $usernameErr = $passErr = '';
    extract($_POST);
    if (empty($username)) {
      $usernameErr = "username is Required";
    } else {
      $usernameErr = true;
    }

    if (empty($password)) {
      $passErr = "Password is Required";
    } else {
      $passErr = true;
    }

    if ($usernameErr == 1 && $passErr == 1) {
      $users->login($username, $password);
    }
  }
}
require_once './templates/header.php';
?>
<div class="form">
  <?php
  if (isset($_GET['messsage'])) :
  ?>
    <p class="<?= $log ? 'message success' : 'message err';  ?>"><?= $_GET['messsage']; ?></p>
  <?php
  endif;
  ?>
  <p class="sign" align="center">Log in</p>
  <form class="form1" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <?php if ($usernameErr != 1) : ?>
      <p class="err-msg err"><?= $usernameErr ?></p>
    <?php endif; ?>
    <input class="input " name="username" type="text" align="center" placeholder="Username">
    <?php if ($passErr != 1) : ?>
      <p class="err-msg err"><?= $passErr ?></p>
    <?php endif; ?>
    <input class="input" name="password" type="password" align="center" placeholder="Password">
    <input type="submit" name="loginSumb" value="Login" class="submit" align="center">
    <p class="signup" align="center"><a href="./register.php">Dont have account?
        <span class="">Register</span>
    </p>
</div>
<?php
require_once './templates/footer.php';
?>