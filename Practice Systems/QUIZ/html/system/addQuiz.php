<?php
require_once './functions.php';

if (isset($_GET['id'])) {
  $quizData = $quizzes->getDatabyId($_GET['id']);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {

  $valid = $titleErr = $descErr = '';
  extract($_POST);
  if (empty($title)) {
    $titleErr = "title is Required";
  } else {
    $titleErr = true;
  }

  if (empty($description)) {
    $descErr = "description is Required";
  } else {
    $descErr = true;
  }

  if ($titleErr == 1 && $descErr == 1) {
    if ($id != null) {
      $quizzes->editQuiz($id, $title, $description);
    } else {
      $quizzes->addQuiz($title, $description);
    }
    header('Location:listQuiz.php');
    exit;
  }
}
require_once './templates/header.php';
require_once './templates/navigation.php';
?>

<div class="form">

  <p class="sign" align="center"><?php if (isset($_GET['id'])) {
                                    echo 'Edit Quiz';
                                  } else {
                                    echo 'Add Quiz';
                                  }  ?></p>
  <form class="form1" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <input type="hidden" name="id" value="<?php if (isset($_GET['id'])) {
                                            echo $quizData['id'];
                                          } ?>" />
    <input class=" input" value="<?php if (isset($_GET['id'])) {
                                    echo $quizData['title'];
                                  } ?>" name="title" type="text" align="center" placeholder="Title">

    <textarea class="input" name="description" placeholder="Description"><?php if (isset($_GET['id'])) {
                                                                            echo $quizData['description'];
                                                                          } ?></textarea>
    <input type="submit" value="Save" class="submit" align="center">

</div>

<?php require_once './templates/footer.php'; ?>