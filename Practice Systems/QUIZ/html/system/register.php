<?php
require_once './register_validation.php';
require_once './templates/header.php';
?>
<div class="form">
  <?php echo $valid; ?>
  <p class="sign" align="center">Register</p>
  <form class="form1" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <?php if ($fnameErr != 1) : ?>
      <p class="err-msg err"><?= $fnameErr ?></p>
    <?php endif; ?>
    <input class="input " name="firstname" type="text" align="center" placeholder="First Name">
    <?php if ($lnameErr != 1) : ?>
      <p class="err-msg err"><?= $lnameErr ?></p>
    <?php endif; ?>
    <input class="input " name="lastname" type="text" align="center" placeholder="Last Name">
    <?php if ($usernameErr != 1) : ?>
      <p class="err-msg err"><?= $usernameErr ?></p>
    <?php endif; ?>
    <input class="input " name="username" type="text" align="center" placeholder="Username">
    <?php if ($passErr != 1) : ?>
      <p class="err-msg err"><?= $passErr ?></p>
    <?php endif; ?>
    <input class="input" name="password" type="password" align="center" placeholder="Password">
    <?php if ($cpassErr != 1) : ?>
      <p class="err-msg err"><?= $cpassErr ?></p>
    <?php endif; ?>
    <input class="input" name="confirmPass" type="password" align="center" placeholder="Confirm Password">
    <input type="submit" value="Submit" class="submit" align="center">
    <p class="signup" align="center"><a href="./login.php">Already have an account?
        <span class="">go to login</span>
    </p>
</div>
<?php
require_once './templates/footer.php';
?>