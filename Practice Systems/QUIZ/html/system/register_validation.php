<?php
require_once './functions.php';

$valid = $usernameErr = $fnameErr = $lnameErr =  $passErr = $cpassErr = '';
$set_firstName = $set_lastName = $set_userName;
extract($_POST);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $validName = "/^[a-zA-Z ]*$/";
  $uppercasePassword = "/(?=.*?[A-Z])/";
  $lowercasePassword = "/(?=.*?[a-z])/";
  $digitPassword = "/(?=.*?[0-9])/";
  $spacesPassword = "/^$|\s+/";
  $symbolPassword = "/(?=.*?[#?!@$%^&*-])/";
  $minEightPassword = "/.{8,}/";

  if (empty($firstname)) {
    $fnameErr = "First Name is Required";
  } else if (!preg_match($validName, $firstname)) {
    $fnameErr = "invalid input";
  } else {
    $fnameErr = true;
  }

  if (empty($username)) {
    $usernameErr = "username is Required";
  } else if ($users->hasAlreadyUsername($username)) {
    $usernameErr = "username already taken";
  } else {
    $usernameErr = true;
  }

  if (empty($lastname)) {
    $lnameErr = "Last Name is Required";
  } else if (!preg_match($validName, $lastname)) {
    $lnameErr = "invalid input";
  } else {
    $lnameErr = true;
  }

  if (empty($password)) {
    $passErr = "Password is Required";
  } elseif (!preg_match($uppercasePassword, $password) || !preg_match($lowercasePassword, $password) || !preg_match($digitPassword, $password) || !preg_match($symbolPassword, $password) || !preg_match($minEightPassword, $password) || preg_match($spacesPassword, $password)) {
    $passErr = "Password must be at least one uppercase letter, lowercase letter, digit, a special character with no spaces and minimum 8 length";
  } else {
    $passErr = true;
  }

  if ($confirmPass != $password) {
    $cpassErr = "Confirm Password doest Matched";
  } else {
    $cpassErr = true;
  }

  if ($fnameErr == 1 && $lnameErr == 1 && $usernameErr == 1 && $passErr == 1 && $cpassErr == 1) {

    $firstName = validate($firstname);
    $lastName =  validate($lastname);
    $password =  validate($password);

    $users->addToUsers($firstName, $lastName, $username, $password);

    header("Location:login.php?messsage=Succcessful registered", true, 301);
    exit();
  } else {
    $set_firstName = $firstname;
    $set_lastName = $lastname;
    $set_email =    $emailAdd;
    $set_userName = $username;
  }
}

function validate($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
