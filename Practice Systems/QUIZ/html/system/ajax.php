<?php

require_once './database/DBController.php';
require_once './database/User.php';
require_once './database/Quiz.php';
require_once './database/QuizQuestion.php';
require_once './database/QuizChoice.php';

$db = new DBController();

$users =  new User($db);
$quizzes = new Quiz($db);
$choicedb = new QuizChoice($db);
$questiondb = new QuizQuestion($db);


if (isset($_POST['searchQuiz'])) {
  $searchKey = $_POST['searchQuiz'];

  $results = $quizzes->getDataSearch($searchKey);

  echo json_encode($results);
}

if (isset($_POST['searchQuestion'])) {
  $searchKey = $_POST['searchQuestion'];
  $quizId = $_POST['quizId'];

  $results = $questiondb->getDataSearch($searchKey, $quizId);

  echo json_encode($results);
}
