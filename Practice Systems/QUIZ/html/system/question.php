<?php
require_once './functions.php';
require_once './templates/header.php';
require_once './templates/navigation.php';

$quizId = $_GET['id'];
$quizData = $quizzes->getDatabyId($quizId);
$questionList = $questiondb->getDatabyQuizId($quizId);
?>

<h2>List of Question for <?= $quizData['title'] ?></h2>
<div class="searchBox">
  <div class="searchbar">
    <input id="searchQuestion" type="text" placeholder="Search..." style="width: 100%;" />
    <button onclick="searchQuestion()" id="searchBtnQuestion" value="<?= $quizId ?>">Search</button>
  </div>
  <div class="actionContainer">
    <a class="btnAdd" href="./addQuestion.php?id=<?= $quizId ?>">Add Question</a>
  </div>
</div>


<div class="table-wrapper">
  <table class="fl-table">
    <thead>
      <tr>
        <th>Question</th>
        <th>choice 1</th>
        <th>choice 2</th>
        <th>choice 3</th>
        <th>Correct answer</th>
        <th>Action</th>
      </tr>
    </thead>

    <tbody id="tableBody">

      <?php foreach ($questionList as $key => $value) : ?>

        <tr>
          <td><?= $value['question'] ?></td>

          <td>
            <?= $value['opt1'] ?>
          </td>
          <td>
            <?= $value['opt2'] ?>
          </td>
          <td>
            <?= $value['opt3'] ?>
          </td>
          <td>
            <?= $value['correct_answer'] ?>
          </td>
          <td class="actions-buttons">
            <a class="editBtn" href="addQuestion.php?id=<?= $quizId ?>&question_id=<?= $value['id'] ?>">Edit</a>
            <button value="<?= $value['id'] ?>" id="deleteButton" onclick="deleteQuestion()" class="delBtn">Delete</button>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>

  </table>
</div>

<?php require_once './templates/footer.php'; ?>