<?php
require_once './functions.php';
require_once './templates/header.php';
require_once './templates/navigation.php';

$listQuiz = $quizzes->getData();

?>
<div class="cards">
  <?php array_map(function ($quiz) { ?>
    <div class="card">
      <div class="cardHeader">
        <p><?= $quiz['title'] ?></p>
      </div>
      <div class="cardContainer">
        <p><?= $quiz['description'] ?></p>
      </div>
      <a class="takeQuizBtn" href="./takequiz.php?id=<?= $quiz['id']; ?>">Take Quiz</a>
    </div>
  <?php }, $listQuiz) ?>

</div>

<?php require_once './templates/footer.php'; ?>