<?php
require_once './functions.php';
require_once './templates/header.php';
require_once './templates/navigation.php';

$listQuiz = $quizzes->getData();

?>
<h2>List of Quizzes</h2>
<div class="searchBox">
  <div class="searchbar">
    <input id="searchQuiz" type="text" placeholder="Search..." style="width: 100%;" />
    <button onclick="searchQuiz()">Search</button>
  </div>
  <div class="actionContainer">
    <a class="btnAdd" href="./addQuiz.php">Add Quiz</a>
  </div>
</div>


<div class="table-wrapper">
  <table class="fl-table">
    <thead>
      <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>

    <tbody id="tableBody">
      <?php array_map(function ($listQuiz) { ?>
        <tr>
          <td><?= $listQuiz['title'] ?></td>
          <td><?= $listQuiz['description'] ?></td>
          <td class="actions-buttons">
            <a class="editBtn" href="question.php?id=<?= $listQuiz['id'] ?>">View Questions</a>
            <a class="editBtn" href="addQuiz.php?id=<?= $listQuiz['id'] ?>">Edit</a>
            <button value="<?= $listQuiz['id'] ?>" id="deleteButton" onclick="deleteQuiz()" class="delBtn">Delete</button>
          </td>
        </tr>
      <?php }, $listQuiz) ?>
    </tbody>

  </table>
</div>

<?php require_once './templates/footer.php'; ?>