<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
  const deleteButton = document.getElementById('deleteButton');
  const searchButton = document.getElementById('searchBtn');



  function deleteQuiz() {
    let valuebtnId = deleteButton.value;
    let text = "Do you want to delete this Quiz?";
    if (confirm(text) == true) {

      window.location.replace(`deleteQuiz.php?id=${valuebtnId}`);
    }
  }

  function deleteQuestion() {
    let valuebtnId = deleteButton.value;
    let text = "Do you want to delete this Question?";
    if (confirm(text) == true) {

      window.location.replace(`deleteQuiz.php?id=${valuebtnId}`);
    }
  }

  function searchQuiz() {
    let searchValue = document.getElementById('searchQuiz').value;

    $.ajax({
      type: "post",
      url: "ajax.php",
      data: {
        searchQuiz: searchValue,
      },
      success: function(data) {
        $('#tableBody').empty();

        let obj = jQuery.parseJSON(data);

        for (let key in obj) {
          let val = obj[key];
          $('#tableBody').append(`  
          <tr>
          <td>${val.title}</td>
          <td>${val.description}</td>
          <td class="actions-buttons">
            <a class="editBtn" href="question.php?id=${val.id}">View Questions</a>
            <a class="editBtn" href="addQuiz.php?id=${val.id}">Edit</a>
            <button value="${val.id}" id="deleteButton" onclick="deleteQuiz()" class="delBtn">Delete</button>
          </td>
        </tr>`);
        }
      }
    });
  }

  function searchQuestion() {
    let searchValue = document.getElementById('searchQuestion').value;
    let quizId = document.getElementById('searchBtnQuestion').value;

    $.ajax({
      type: "post",
      url: "ajax.php",
      data: {
        searchQuestion: searchValue,
        quizId: quizId,
      },
      success: function(data) {
        $('#tableBody').empty();

        let obj = jQuery.parseJSON(data);

        for (let key in obj) {
          let val = obj[key];
          $('#tableBody').append(`  
          <tr>
          <td>${val.question}</td>

          <td>
          ${val.opt1}
          </td>
          <td>
          ${val.opt2}
          </td>
          <td>
          ${val.opt3}
          </td>
          <td>
          ${val.correct_answer}
          </td>
          <td class="actions-buttons">
            <a class="editBtn" href="addQuestion.php?id=${quizId}&question_id=${val.id} ?>">Edit</a>
            <button value="${val.id}" id="deleteButton" onclick="deleteQuestion()" class="delBtn">Delete</button>
          </td>
        </tr>`);
        }
      }
    });
  }
</script>
</body>

</html>