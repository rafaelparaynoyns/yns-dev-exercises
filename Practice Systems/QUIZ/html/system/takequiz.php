<?php
require_once './functions.php';

$ID = $_GET['id'];


$questionsList = $questiondb->getDatabyQuizId($ID);

shuffle($questionsList);
$i = 0;
require_once './templates/header.php';
require_once './templates/navigation.php';
?>
<div class="takequiz">
  <form method="POST" action="./result.php?qno=<?= count($questionsList) ?>">
    <?php foreach ($questionsList as $key => $value) : ?>

      <div>
        <h2 style="text-align: center;"><?= $i + 1 ?>. <?= $value['question'] ?></h2>
        <input type="radio" name="choice<?= $i ?>" value="<?= $value['opt1'] ?>" required /><?= $value['opt1'] ?>
        <input type="radio" name="choice<?= $i ?>" value="<?= $value['opt2'] ?>" required /><?= $value['opt2'] ?>
        <input type="radio" name="choice<?= $i ?>" value="<?= $value['opt3'] ?>" required /><?= $value['opt3'] ?>
        <input type="hidden" name="correct_answer<?= $i ?>" value="<?= $value['correct_answer'] ?>" />
      </div>

    <?php $i++;
    endforeach; ?>
    <div style="text-align: center;margin-top: 20px;margin-bottom:20px">
      <button type="submit">Submit answer</button>
    </div>

  </form>
</div>

<?php require_once './templates/footer.php'; ?>