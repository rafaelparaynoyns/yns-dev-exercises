<?php

require_once './database/DBController.php';
require_once './database/User.php';
require_once './database/Quiz.php';
require_once './database/QuizQuestion.php';
require_once './database/QuizChoice.php';

$db = new DBController();

$users =  new User($db);
$quizzes = new Quiz($db);
$choicedb = new QuizChoice($db);
$questiondb = new QuizQuestion($db);
