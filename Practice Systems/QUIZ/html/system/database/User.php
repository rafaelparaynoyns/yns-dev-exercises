<?php

class User
{

  public $db = null;

  public function __construct(DBController $db)
  {
    if (!isset($db->con)) return null;
    $this->db = $db;
  }

  public function addToUsers($firstname, $lastname, $username, $password)
  {

    $cryptsPass =  password_hash($password, PASSWORD_DEFAULT);
    $params = array(
      'username' => "'{$username}'",
      'password' => "'{$cryptsPass}'",
      'first_name' => "'$firstname'",
      'last_name' => "'$lastname'"
    );

    $result =  $this->insertData($params);
    return $result;
  }


  public function insertData($params = null, $table = "users")
  {
    if ($this->db->con != null) {
      if ($params != null) {

        $columns = implode(',', array_keys($params));

        $values = implode(',', array_values($params));

        $query_string = sprintf("INSERT INTO %s(%s) VALUES(%s)", $table, $columns, $values);

        $result = $this->db->con->query($query_string);
        return $result;
      }
    }
  }


  public function login($username, $password)
  {
    if ($this->db->con != null) {

      $args = $this->get_pwd_from_info($username);
      $passwordfromdb = $args['password'];

      if (password_verify($password, $passwordfromdb)) {
        session_start();
        $_SESSION['user'] = $username;;
        header('Location:dashboard.php');
        exit;
      } else {
        header("Location:login.php?messsage=Incorrect Username or Password", true, 301);
        exit;
      }
    }
  }

  public function hasAlreadyUsername($username)
  {
    $result = $this->db->con->query("SELECT * FROM users WHERE username = '{$username}' ");
    return mysqli_num_rows($result);
  }

  private function get_pwd_from_info($username)
  {
    $result = $this->db->con->query("SELECT * FROM users WHERE username = '{$username}' ");
    $args = mysqli_fetch_array($result);

    return $args;
  }
}
