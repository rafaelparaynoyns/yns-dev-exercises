<?php


class QuizChoice
{
  public $db = null;

  public function __construct(DBController $db)
  {
    if (!isset($db->con)) return null;
    $this->db = $db;
  }

  public function getData()
  {
    $result = $this->db->con->query("SELECT * FROM question_choice");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function getDatabyId($id)
  {
    $result = $this->db->con->query("SELECT * FROM question_choice WHERE id = {$id} ");

    $result = mysqli_fetch_array($result, MYSQLI_ASSOC);



    return $result;
  }

  public function getDatabyQuestionId($id)
  {
    $result = $this->db->con->query("SELECT * FROM question_choice WHERE quiz_question_id = {$id} ");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function insertData($params = null, $table = "question_choice")
  {
    if ($this->db->con != null) {
      if ($params != null) {

        $columns = implode(',', array_keys($params));

        $values = implode(',', array_values($params));

        $query_string = sprintf("INSERT INTO %s(%s) VALUES(%s)", $table, $columns, $values);


        $result = $this->db->con->query($query_string);
        return $result;
      }
    }
  }

  public function addChoice($question_id, $choice, $is_correct)
  {

    $params = array(
      'quiz_question_id' => "{$question_id}",
      'choice' => "'{$choice}'",
      'is_correct' => "{$is_correct}"
    );


    $this->insertData($params);
  }


  public function editChoice($id, $title, $desc)
  {
    $queryString = "UPDATE question_choice SET choice = '{$title}' WHERE id = {$id}";

    $this->db->con->query($queryString);
  }

  public function deleteChoice($id)
  {

    $queryString = "DELETE FROM question_choice WHERE id = {$id}";

    $result = $this->db->con->query($queryString);
    return $result;
  }
}
