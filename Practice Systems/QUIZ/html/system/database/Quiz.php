<?php


class Quiz
{
  public $db = null;

  public function __construct(DBController $db)
  {
    if (!isset($db->con)) return null;
    $this->db = $db;
  }

  public function getData()
  {
    $result = $this->db->con->query("SELECT * FROM quiz");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function getDatabyId($id)
  {
    $result = $this->db->con->query("SELECT * FROM quiz WHERE id = {$id} ");

    $result = mysqli_fetch_array($result, MYSQLI_ASSOC);


    return $result;
  }

  public function getDataSearch($searchKey)
  {
    $result = $this->db->con->query("SELECT * FROM quiz WHERE title LIKE '%{$searchKey}%' 
                                        OR description LIKE '%{$searchKey}%' ");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function insertData($params = null, $table = "quiz")
  {
    if ($this->db->con != null) {
      if ($params != null) {

        $columns = implode(',', array_keys($params));

        $values = implode(',', array_values($params));

        $query_string = sprintf("INSERT INTO %s(%s) VALUES(%s)", $table, $columns, $values);


        $result = $this->db->con->query($query_string);
        return $result;
      }
    }
  }

  public function addQuiz($title, $desc)
  {

    $params = array(
      'title' => "'{$title}'",
      'description' => "'{$desc}'"
    );


    $result = $this->insertData($params);
  }


  public function editQuiz($id, $title, $desc)
  {
    $queryString = "UPDATE quiz SET title = '{$title}', description = '{$desc}' WHERE id = {$id}";

    $this->db->con->query($queryString);
  }



  public function deleteQuiz($id)
  {

    $queryString = "DELETE FROM quiz WHERE id = {$id}";

    $result = $this->db->con->query($queryString);
    return $result;
  }
}
