<?php


class QuizQuestion
{
  public $db = null;

  public function __construct(DBController $db)
  {
    if (!isset($db->con)) return null;
    $this->db = $db;
  }

  public function getData()
  {
    $result = $this->db->con->query("SELECT * FROM quiz_question");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function getDatabyId($id)
  {
    $result = $this->db->con->query("SELECT * FROM quiz_question WHERE id = {$id} ");

    $result = mysqli_fetch_array($result, MYSQLI_ASSOC);

    return $result;
  }

  public function getDatabyQuizId($id)
  {
    $result = $this->db->con->query("SELECT * FROM quiz_question WHERE quiz_id = {$id} ");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function getDataSearch($searchKey, $id)
  {
    $result = $this->db->con->query("SELECT * FROM quiz_question WHERE quiz_id = {$id} AND question LIKE '%{$searchKey}%'");

    $resultArray = array();

    while ($item = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $resultArray[] = $item;
    }

    return $resultArray;
  }

  public function insertData($params = null, $table = "quiz_question")
  {
    if ($this->db->con != null) {
      if ($params != null) {

        $columns = implode(',', array_keys($params));

        $values = implode(',', array_values($params));

        $query_string = sprintf("INSERT INTO %s(%s) VALUES(%s)", $table, $columns, $values);

        $this->db->con->query($query_string);
      }
    }
  }

  public function addQuiz_question($quiz_id, $question, $opt1, $opt2, $opt3, $correct_answer)
  {

    $params = array(
      'quiz_id' => "{$quiz_id}",
      'question' => "'{$question}'",
      'opt1' => "'{$opt1}'",
      'opt2' => "'{$opt2}'",
      'opt3' => "'{$opt3}'",
      'correct_answer' => "'{$correct_answer}'"
    );

    $this->insertData($params);
  }


  public function editQuiz_question($id, $question, $opt1, $opt2, $opt3, $correct_answer)
  {
    $queryString = "UPDATE quiz_question SET question = '{$question}',
                  opt1 = '{$opt1}', opt2 = '{$opt2}', opt3 = '$opt3' , correct_answer = '$correct_answer' 
                  WHERE id = {$id}";

    $this->db->con->query($queryString);
  }

  public function deleteQuiz_question($id)
  {

    $queryString = "DELETE FROM quiz_question WHERE id = {$id}";

    $result = $this->db->con->query($queryString);
    return $result;
  }
}
