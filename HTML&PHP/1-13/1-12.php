<?php
$csvFiles = [];
$currentArrayPage = [];

if (file_exists("data.csv")) {
  $file = fopen('data.csv', 'r');

  while (($line = fgetcsv($file)) !== false) {
    array_push($csvFiles, $line);
  }

  fclose($file);

  $currentPage = $_GET['page'] ?? 1;
  $total = count($csvFiles);
  $page = ceil($total / 10);
  $currentArrayPage = array_slice($csvFiles, ($currentPage - 1) * 10, 10);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">`
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./style.css">
</head>

<body>
  <div class="table-wrapper">
    <table class="fl-table">
      <thead>
        <tr>
          <th>First name</th>
          <th>Last name</th>
          <th>Age</th>
          <th>Email</th>
          <th>Display Image</th>
        </tr>
      </thead>


      <tbody>

        <?php if (isset($currentArrayPage)) :  ?>
          <?php foreach ($currentArrayPage as $key => $value) : ?>
            <tr>
              <th><?= $value[0]; ?>
              </th>
              <th><?= $value[1]; ?>
              </th>
              <th><?= $value[2]; ?>
              </th>
              <th><?= $value[3]; ?>
              </th>
              <th><img height="100" width="100" src='<?= 'image/' . $value[5] ?>' />
              </th>
            </tr>
        <?php endforeach;
        endif; ?>
      </tbody>
      <div class="pagination">
        Pages:
        <?php for ($i = 1; $i <= $page; $i++) {
          echo '<a href="1-12.php?page=' . $i . '"> ' . $i . '</a>';
        } ?>
      </div>
    </table>
  </div>
</body>

</html>