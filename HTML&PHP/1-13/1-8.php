<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<link rel="stylesheet" href="./style.css">
</style>

<body>
    <?php
    $valid = $fnameErr = $lnameErr = $ageErr = $emailErr = $passErr = $cpassErr = '';
    $set_firstName = $set_age =  $set_lastName = $set_email = '';
    extract($_POST);
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $validName = "/^[a-zA-Z ]*$/";
        $validEmail = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
        $uppercasePassword = "/(?=.*?[A-Z])/";
        $lowercasePassword = "/(?=.*?[a-z])/";
        $digitPassword = "/(?=.*?[0-9])/";
        $spacesPassword = "/^$|\s+/";
        $symbolPassword = "/(?=.*?[#?!@$%^&*-])/";
        $minEightPassword = "/.{8,}/";

        if (empty($firstname)) {
            $fnameErr = "First Name is Required";
        } else if (!preg_match($validName, $firstname)) {
            $fnameErr = "invalid input";
        } else {
            $fnameErr = true;
        }

        if (empty($lastname)) {
            $lnameErr = "Last Name is Required";
        } else if (!preg_match($validName, $lastname)) {
            $lnameErr = "invalid input";
        } else {
            $lnameErr = true;
        }

        if (empty($age)) {
            $ageErr = "Age is Required";
        } else {
            $ageErr = true;
        }

        if (empty($emailAdd)) {
            $emailErr = "Email is Required";
        } else if (!preg_match($validEmail, $emailAdd)) {
            $emailErr = "Invalid Email Address";
        } else {
            $emailErr = true;
        }

        if (empty($password)) {
            $passErr = "Password is Required";
        } elseif (!preg_match($uppercasePassword, $password) || !preg_match($lowercasePassword, $password) || !preg_match($digitPassword, $password) || !preg_match($symbolPassword, $password) || !preg_match($minEightPassword, $password) || preg_match($spacesPassword, $password)) {
            $passErr = "Password must be at least one uppercase letter, lowercase letter, digit, a special character with no spaces and minimum 8 length";
        } else {
            $passErr = true;
        }

        if ($confirmPass != $password) {
            $cpassErr = "Confirm Password doest Matched";
        } else {
            $cpassErr = true;
        }

        if ($fnameErr == 1 && $lnameErr == 1 && $emailErr == 1 && $passErr == 1 && $cpassErr == 1) {
           


            $firstName = validate($firstname);
            $lastName =  validate($lastname);
            $email =     validate($emailAdd);
            $password =  validate($password);

            $data = array(
                $firstName,
                $lastName,
                $email,
                $age,
                password_hash($password, PASSWORD_DEFAULT),
            );
            if (file_exists("data.csv")) {
                $file = fopen("data.csv", "a");
            } else {
                $file = fopen("data.csv", "w");
            }
            fputcsv($file, $data);
            if (fclose($file)) {
                $valid = "All fields are validated successfully";
            }
        } else {
            $set_firstName = $firstname;
            $set_lastName = $lastname;
            $set_email =    $emailAdd;
        }
    }

    function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    ?>
    <div>
        <?php echo $valid; ?>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="fname">First Name</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($fnameErr != 1) {
                    echo $fnameErr;
                } ?>
            </p>
            <input type="text" id="fname" name="firstname" placeholder="First Name" value="<?php if (isset($firstname)) echo $firstname ?>" />


            <label for="lname">Last Name</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($lnameErr != 1) {
                    echo $lnameErr;
                } ?>
            </p>
            <input type="text" id="lname" name="lastname" placeholder="Last Name" value="<?php if (isset($lastname)) echo $lastname ?>" />

            <label for="age">Age</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($ageErr != 1) {
                    echo $ageErr;
                } ?>
            </p>
            <input type="number" id="age" name="age" placeholder="Age" value="<?php if (isset($age)) echo $age ?>" />

            <label for="emailAdd">Email</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($emailErr != 1) {
                    echo $emailErr;
                } ?>
            </p>
            <input type="email" id="emailAdd" name="emailAdd" placeholder="Email" value="<?php if (isset($emailAdd)) echo $emailAdd ?>" />

            <label for="password">Password</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($passErr != 1) {
                    echo $passErr;
                } ?>
            </p>
            <input type="password" id="password" name="password" placeholder="Password" value="<?php if (isset($password)) echo $password ?>" />

            <label for="confirmPass">Confirm Password</label><span class="required">*</span>
            <p class="err-msg">
                <?php if ($cpassErr != 1) {
                    echo $cpassErr;
                } ?>
            </p>
            <input type="password" id="confirmPass" name="confirmPass" placeholder="Confirm Password" value="<?php if (isset($confirmPassword)) echo $confirmPassword ?>" />

            <input type="submit" value="submit">

        </form>
    </div>
</body>

</html>