<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">`
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <div class="table-wrapper">
        <table class="fl-table">
            <thead>
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Age</th>
                    <th>Email</th>
                </tr>
            </thead>


            <tbody>
                <tr>
                    <?php

                    if (file_exists("data.csv")) {
                        $file = fopen("data.csv", "r");

                        while (!feof($file)) {

                            $data = fgetcsv($file);

                            if (!empty($data)) {
                                echo
                                "<tr>
                                <td>$data[0]</td>
                                <td>$data[1]</td>
                                <td>$data[3]</td>
                                <td>$data[2]</td>
                            </tr>";
                            }
                        }

                        fclose($file);
                    }

                    ?>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>