<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<link rel="stylesheet" href="./style.css">
<body>
    <div>
        <form action="./output.php">
            <label for="fname">First Name</label>
            <input type="text" id="fname" name="firstname" placeholder="First Name" value="<?= $_POST['firstname'] ?>">

            <label for="mname">Middle Name</label>
            <input type="text" id="mname" name="middlename" placeholder="Midde Name" value="<?= $_POST['middlename'] ?>">

            <label for="lname">Last Name</label>
            <input type="text" id="lname" name="lastname" placeholder="Last Name" value="<?= $_POST['lastname'] ?>">

            <label for="age">Age</label>
            <input type="number" id="age" name="age" placeholder="Age" value="<?= $_POST['age'] ?>">

            <label for="emailAdd">Email</label>
            <input type="email" id="emailAdd" name="emailAdd" placeholder="Email" value="<?= $_POST['emailAdd'] ?>">

            <input type="submit" value="Submit">
        </form>
    </div>
</body>

</html>