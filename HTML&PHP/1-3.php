<?php

function gcd($a, $b)
{
    if ($a == 0)
        return $b;
    if ($b == 0)
        return $a;

    if ($a == $b)
        return $a;

    if ($a > $b)
        return gcd($a - $b, $b);

    return gcd($a, $b - $a);
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form>
        <?php
        if (isset($_REQUEST['submitBtn'])) {
            $number1 = $_REQUEST['first_value'];
            $number2 = $_REQUEST['second_value'];
            $gcd = 0;

            $gcd = gcd($number1, $number2);
        }
        ?>
        First Number:<input name="first_value" type="number" value=""><br>
        Second Number:<input name="second_value" type="number" value=""><br>
        <input type="submit" name="submitBtn">
        <br>Result:
        <?php
        if (isset($gcd)) {
            echo $gcd;
        }
        ?>
    </form>
</body>

</html>