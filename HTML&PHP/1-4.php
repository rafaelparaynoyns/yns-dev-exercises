<?php

function fizzBuzzFunc($a)
{
    for ($i = 1; $i <= $a; $i++) {
        if ($i % 15 == 0) {
            echo 'FizzBuzz<br>';
        } elseif ($i % 3 == 0) {
            echo 'Fizz<br>';
        } elseif ($i % 5 == 0) {
            echo 'Buzz<br>';
        } else {
            echo $i . '<br>';
        }
    }
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form>
        <?php
        if (isset($_REQUEST['submitBtn'])) {
            $number1 = $_REQUEST['first_value'];
        }
        ?>
        First Number:<input name="first_value" type="number" value=""><br>
        <input type="submit" name="submitBtn">
        <br>Result:
        <?php
        if (isset($number1)) {
            fizzBuzzFunc($number1);
        }
        ?>
    </form>
</body>

</html>