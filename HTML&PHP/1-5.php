<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form>
        <?php
        if (isset($_REQUEST['submitBtn'])) {
            $dateStart = $_REQUEST['dateStart'];
            $dateFormatted = date('F d, Y', strtotime($dateStart));
            $dateEnd = date('F d, Y', strtotime($dateStart . " + 3 days"));
        }
        ?>
        Start Date:<input name="dateStart" type="date" value=""><br>
        <input type="submit" name="submitBtn">
        <br>Result:<br>
        <?php
        if (isset($dateFormatted) && isset($dateEnd)) {
            while ($dateFormatted < $dateEnd) {
              
                $dateFormatted =  date('F d, Y', strtotime($dateFormatted . " + 1 days"));
                echo $dateFormatted . ' ' . date('D', strtotime($dateFormatted)), '<br>';
            }
        } ?>
    </form>
</body>

</html>