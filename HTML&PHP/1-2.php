<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>

<body>
    <?php
    if (isset($_REQUEST['operator'])) {
        $operator = $_REQUEST['operator'];
        $number1 = $_REQUEST['first_value'];
        $number2 = $_REQUEST['second_value'];

        if ($operator == "+") {
            $res = $number1 + $number2;
        }
        if ($operator == "-") {
            $res = $number1 - $number2;
        }
        if ($operator == "X") {
            $res = $number1 * $number2;
        }
        if ($operator == "/") {
            $res = $number1 / $number2;
        }
    }
    ?>
    <form>
        First Number:<input name="first_value" type="number" value=""><br>
        Second Number:<input name="second_value" type="number" value=""><br>
        <input type="submit" name="operator" value="+">
        <input type="submit" name="operator" value="-">
        <input type="submit" name="operator" value="X">
        <input type="submit" name="operator" value="/"><br>
        <br>Result:
        <?php
        if (isset($res)) {
            echo $res;
        }
        ?>
    </form>
</body>

</html>