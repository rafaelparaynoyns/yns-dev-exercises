CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
)

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Exective'),
(2, 'Admin'),
(3, 'Sales'),
(4, 'Development'),
(5, 'Design'),
(6, 'Marketing');

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
)

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'CEO'),
(2, 'CTO'),
(3, 'CFO'),
(4, 'Manager'),
(5, 'Staff');

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `department_id` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int(11) DEFAULT NULL
)

INSERT INTO `employee` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
(1, 'Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL),
(2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
(3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-12-01', 1),
(4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
(5, 'Lorraine', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
(9, 'Hideshi', 'Ogoshi', 'null', '1983-07-15', 4, '2014-06-01', 1),
(10, 'Kim', ' ', ' ', '1977-10-16', 5, '2015-08-06', 1);

CREATE TABLE `employee_positions` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) 

INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 3, 5),
(6, 4, 5),
(7, 5, 5),
(8, 6, 5),
(9, 7, 5),
(10, 8, 5),
(11, 9, 5),
(12, 10, 5);

ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `employee_positions`
  ADD PRIMARY KEY (`id`);


--#1
SELECT * FROM employee
WHERE last_name like "K%"

--#2
SELECT * FROM employee
WHERE last_name  like "%I"

--#3
SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name ) AS full_name, hire_date 
FROM employee
WHERE hire_date BETWEEN '2015-1-1' AND '2015-3-31'
ORDER BY hire_date ASC 

--#4
SELECT emp.last_name as 'Employee',(SELECT last_name  FROM employee as e WHERE e.id = emp.boss_id) 
FROM employee as emp WHERE boss_id IS NOT NULL

--#5
SELECT last_name FROM employee WHERE department_id iN (SELECT department_id FROM departments WHERE department_id = 3 )

--#6 
SELECT count(id) as 'count_has_middle' FROM employee WHERE middle_name is not null AND middle_name != " "

--#7
SELECT name,  (SELECT Count(*) FROM employee as emp WHERE emp.department_id = dep.id) as 'count'
FROM departments as dep
WHERE dep.id in (SELECT e.department_id FROM employee as e)

--#8
SELECT emp.first_name, emp.middle_name, emp.last_name,emp.hire_date 
FROM employee as emp
ORDER BY emp.hire_date ASC 
LIMIT 1

--#9
select dept.name
from departments as dept
where dept.id 
not in (select emp.department_id from employee as emp)
 
--#10
SELECT emp.first_name, emp.middle_name, emp.last_name
FROM employee as emp
WHERE emp.id IN (SELECT pos.id FROM employee_positions as pos)
HAVING COUNT(*) > 2